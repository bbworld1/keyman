# keyman

Keyman is a utility for managing keys.

## Installation

Keyman is installable with pip:

`pip3 install git+https://gitlab.com/bbworld1/keyman --user`

Make sure ~/.local/bin is on PATH; add this to the end of ~/.zshrc or ~/.bashrc -

```zsh
export PATH=$HOME/.local/bin:$PATH
```

## Usage

`keyman --help` will show a usage message.

`keyman list` lists managed SSH keys.
`keyman create` creates a new managed SSH key.
`keyman delete` deletes an SSH key.
`keyman switch <name>` changes the active key (id_rsa).
`keyman pub <name>` shows the pubkey for the specified key.
