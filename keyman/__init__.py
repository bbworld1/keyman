import os
import sys
import shutil
from pathlib import Path
import configparser
import click

# TODO: Organize this block
SSH_PATH = Path.home() / ".ssh"
ID_RSA = ""

VERSION = "1.0.0"

@click.group(help="Keyman is a small utility for SSH key management.\nConfiguration is located at ~/.config/keyman.conf.")
def cli():
    global SSH_PATH
    global ID_RSA
    config = configparser.ConfigParser()
    (Path.home() / ".config").mkdir(exist_ok=True)
    (Path.home() / ".config" / "keyman.conf").touch()
    config.read(Path.home() / ".config" / "keyman.conf")
    if "DEFAULT" not in config:
        config["DEFAULT"] = { "ssh_path": str(Path.home() / ".ssh") }
        with open(Path.home() / ".config" / "keyman.conf", "w") as f:
            config.write(f)
    SSH_PATH = Path(config["DEFAULT"].get("ssh_path", str(Path.home() / ".ssh")))
    ID_RSA = ""
    if not SSH_PATH.exists():
        click.echo("SSH Key directory path does not exist, exiting now.")
        sys.exit(1)
    if not (SSH_PATH / "id_rsa").exists():
        proceed = input("No key found in this directory. Proceed? [y/n]: ")
        if not proceed.lower() == "y":
            sys.exit(1)
    else:
        with open(SSH_PATH / "id_rsa") as f:
            ID_RSA = f.read()

@cli.command()
@click.argument("key_name")
def switch(key_name):
    """
    Switch keys to named key.
    """
    key_path = SSH_PATH / f"id_rsa_{key_name}"
    if not key_path.is_file():
        click.echo(click.style(f"Key {key_name} does not exist.", fg="red"))
        sys.exit(1)
    try:
        shutil.copy(key_path, SSH_PATH / "id_rsa")
    except shutil.SameFileError:
        pass
    click.echo(click.style("Switched key successfully!", fg="green"))

@cli.command()
@click.argument("key_name")
def create(key_name):
    """
    Create a new key pair with specified name.
    """
    global ID_RSA
    key_path = SSH_PATH / f"id_rsa_{key_name}"
    key_path_pub = SSH_PATH / f"id_rsa_{key_name}.pub"
    os.system(f"ssh-keygen -f {key_path}")
    if not key_path_pub.exists():
        click.echo(click.style(f"Key does not appear to have been created.", fg="red"))
        sys.exit(1)
    with open(key_path_pub) as f:
        click.echo(click.style("Key created! Here's the pubkey:", fg="green"))
        click.echo()
        click.echo(f.read())
    if ID_RSA == "":
        shutil.copy(key_path, SSH_PATH / "id_rsa")
        with open(key_path) as f:
            ID_RSA = f.read()

@cli.command()
@click.argument("key_name")
def secret(key_name):
    """
    Print secret private key text.
    """
    key_path = SSH_PATH / f"id_rsa_{key_name}"
    if not key_path.is_file():
        click.echo(click.style(f"Key {key_name} does not exist.", fg="red"))
        sys.exit(1)
    with open(key_path) as f:
        click.echo(f.read())

@cli.command()
@click.argument("key_name")
def pub(key_name):
    """
    Print public key.
    """
    key_path = SSH_PATH / f"id_rsa_{key_name}.pub"
    if not key_path.is_file():
        click.echo(click.style(f"Key {key_name} does not exist or does not have an associated pubkey.", fg="red"))
        sys.exit(1)
    with open(key_path) as f:
        click.echo(f.read())

@cli.command()
@click.argument("key_name")
def delete(key_name):
    """
    Delete key.
    """
    key_path = SSH_PATH / f"id_rsa_{key_name}"
    pubkey_path = SSH_PATH / f"id_rsa_{key_name}.pub"
    if not key_path.is_file():
        click.echo(click.style(f"Key {key_name} does not exist.", fg="red"))
        sys.exit(1)
    key_path.unlink()
    try:
        pubkey_path.unlink()
    except FileNotFoundError:
        pass

@cli.command()
def list():
    """
    List available keys.
    """
    c = 0
    for path in SSH_PATH.iterdir():
        if not path.is_file(): continue
        if not path.name.startswith("id_rsa_"): continue
        if path.name.endswith(".pub"): continue
        match = False
        with open(path) as f:
            data = f.read()
            if data == ID_RSA:
                match = True
        click.echo(click.style(path.name.split("id_rsa_")[1], fg="blue") + (click.style(" (active)", fg="blue", bold=True) if match else "" ))
        c += 1
    if c == 0:
        click.echo(click.style("No keys found. Use keyman create to make one.", fg="blue"))

if __name__ == "__main__":
    cli()
